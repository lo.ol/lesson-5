#include "Polygon.h"

Polygon::Polygon(const string & name, const string & type) : Shape(name, type)
{

}

Polygon::~Polygon()
{
	this->_points.clear();
	
}

/*moves all points on points vector by other point*/
void Polygon::move(const Point & other)
{
	for (int i = 0; i < _points.size(); i++)
	{
		_points[i] += other;
	}
}



