#pragma once

#include "Shape.h"
#include "Point.h"
#include <iostream>
#include <vector>
#include "Cimg.h"

using namespace std;

class Polygon : public Shape
{
public:
	Polygon(const string& name, const string& type);
	virtual ~Polygon();

	// override functions if need (virtual + pure virtual)
	virtual void move(const Point& other);

protected:
	vector<Point> _points;
};