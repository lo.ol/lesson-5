#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point & a, double length, double width, const string & type, const string & name) : Polygon(name, type) 
{ 
	if (width <= 0 || length <= 0) //in case there is no right length or width rectangle wont be created
	{
		cout << "Length or Width can't be 0." << endl;
		(*this).~Rectangle();
	}
	else
	{
		this->_length = length;
		this->_width = width;
		_points.push_back(a);
		Point b(length - a.getX(), width - a.getY()); //create another point to draw rectangle
		_points.push_back(b);
	}
	

}

myShapes::Rectangle::~Rectangle() 
{
	
}


void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


double myShapes::Rectangle::getArea() const
{
	return _width * _length;
}

double myShapes::Rectangle::getPerimeter() const
{
	return 2*(_length + _width);
}


