#include "Shape.h"
using namespace std;

Shape::Shape(const string & name, const string & type)
{
	this->_name = name;
	this->_type = type;
}

/*prints type name area and perimeter of shape*/
void Shape::printDetails() const
{
	cout << _type << "  " << _name << "       " << getArea() <<"    " << getPerimeter() << endl;
	
}

string Shape::getType() const
{

	return _type;
}

string Shape::getName() const
{
	return _name;
}
