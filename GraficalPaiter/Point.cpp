#include "Point.h"

Point::Point(double x, double y) : _x(x), _y(y)
{
	
}

Point::Point(const Point & other)
{
	this->_x = other._x;
	this->_y = other._y;
}

Point::~Point()
{

}

/*adding point to another point*/
Point Point::operator+(const Point & other) const
{
	Point newPoint(_x + other._x, _y + other._y);

	return newPoint;
}


Point & Point::operator+=(const Point & other)
{
	this->_x = _x + other._x;
	this->_y = _y + other._y;
	return *this;
}

double Point::getX() const
{
	return _x;
}

double Point::getY() const
{
	return _y;
}

/*calculating distance*/
double Point::distance(const Point & other) const
{
	double x = _x - other._x;
	double y = _y - other._y;
	x = pow(x, 2);
	y = pow(y, 2);


	return sqrt(x + y);
}
