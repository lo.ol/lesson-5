#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:

	Menu();
	~Menu();

	void menu();
// more functions..
	void creatCircle();

	void createArrow();

	void setName(string& name);
	
	vector<Point> getPoints(int num);
	void createTriangle();
	void createRectangle();
	void moveShape(int index);

	void removeShape(int index);

	void deleteAll();
	

private: 
	vector<Shape*> _shapes;
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

