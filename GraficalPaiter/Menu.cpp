#include "Menu.h"
#define EXIT 3

Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

void Menu::menu()
{
	int choice = 0, choice2 = 0;
	
	
	do
	{
		choice2 = -1;
		choice = -1;
		system("cls");
		cout << "Enter 0 to add a new shape.\nEnter 1 to modify or get information from a current shape.\nEnter 2 to delete all of the shapes.\nEnter 3 to exit." << endl;
		cin >> choice;
		switch (choice) //begining of main switch case
		{
		case 0: //first case of adding a shape
			system("cls");
			cout << "Enter 0 to add a circle.\nEnter 1 to add an arrow.\nEnter 2 to add a triangle.\nEnter 3 to add a rectangle." << endl;
			do {
				cin >> choice2;
			} while (choice2 < 0 || choice2 > 3);
			switch (choice2) //switch case to choose type of a shape to add
			{
			case 0:
				creatCircle();
				break;
			case 1:
				createArrow();
				break;
			case 2:
				createTriangle();
				break;
			case 3:
				createRectangle();
				break;
			default:
				break;
			} //end of shape type switch case
	
			break; //end of first case
		case 1: //begining of case 1
			system("cls");
			if (_shapes.size() != 0)
			{
				//ptints all shapes availible for change
				for (int i = 0; i < _shapes.size(); i++)
				{
					cout << "Enter " << i << " for " << _shapes[i]->getName() << "(" << _shapes[i]->getType() << ")" << endl;
				}
				do {
					cin >> choice; //gets namber of shape to modify
				} while (choice < 0 || choice > _shapes.size());


				cout << "Enter 0 to move the shape\nEnter 1 to get its details.\nEnter 2 to remove the shape." << endl;
				do {
					cin >> choice2; //gets the kind of action to do
				} while (choice2 < 0 || choice2 > 2);
				switch (choice2) //switch case to functions that can be done on some shape
				{
					system("cls");
				case 0:
					moveShape(choice);
					break;
				case 1:
					_shapes[choice]->printDetails();
					system("pause");
					break;
				case 2:
					removeShape(choice);
					break; //end of functions switch case
				}
			}
			
			break; //end of case 1
		case 2: //begining of case 2
			deleteAll();
			break; //end of case 2
		case 3: //begining of case 3
			for (int i = 0; i < _shapes.size(); i++)
			{
				delete _shapes[i];
			}
			_shapes.clear(); 
			break; //end of case 3
		default:
			break;
		}
		
	} while (choice != EXIT);
	
}

/*function adds a circle to vector shapes*/
void Menu::creatCircle()
{
	double x, y, radius;
	string name, type = "Circle";
	
	cout << "Please enter X :" << endl;
	cin >> x;
	cout << "Please enter Y:" << endl;
	cin >> y;
	cout << "Please enter radius:" << endl;
	cin >> radius;
	setName(name);
	Point p(x, y);
	Shape* newShape = new Circle(p, radius, type, name);
	newShape->draw(*_disp, *_board);
	_shapes.push_back(newShape);
	
}

/*function adds an arrow to vector shapes*/
void Menu::createArrow()
{
	string name = "",type = "Arrow";
	vector<Point> points = getPoints(2);
	setName(name);
	Shape* newShape = new Arrow(points[0], points[1], type, name);
	newShape->draw(*_disp, *_board);
	_shapes.push_back(newShape);
	points.clear();
	
}

/*function gets a name for the shape*/
void Menu::setName(string& name)
{
	cout << "Please enter the name of the shape:" << endl;
	cin >> name;
	
}

/*function gets x and y for points
and creates number of points by num
(used in triangle and arrow)*/
vector<Point> Menu::getPoints(int num)
{
	double x, y;
	vector<Point> points;
	for (int i = 0; i < num; i++)
	{
		cout << "Enter the X of point number: " << i + 1 << endl;
		cin >> x;
		cout << "Enter the Y of point number: " << i + 1 << endl;
		cin >> y;
		points.push_back(Point(x, y));
	}
	return points;
}

/*function adds a triangle to vector shapes*/
void Menu::createTriangle()
{
	vector<Point> points = getPoints(3);
	string name = "", type = "Triangle";
	setName(name);
	Shape* newShape = new Triangle(points[0], points[1], points[2], type, name);
	points.clear();
	if (newShape->getType() != "") //in case all points were in the same line and triangle was not created.
	{
		newShape->draw(*_disp, *_board);
		_shapes.push_back(newShape);
	}
	
}

/*function adds a rectangle to vector shapes*/
void Menu::createRectangle()
{
	string name = "", type = "Rectangle";
	double x, y, len, width;
	cout << "Enter the X of the top left corner: " << endl;
	cin >> x;
	cout << "Enter the Y of the top left corner: " << endl;
	cin >> y;
	Point left(x, y);
	cout << "Please enter the length of the shape: " << endl;
	cin >> len;
	cout << "Please enter the width of the shape: " << endl;
	cin >> width;
	setName(name);
	Shape* newShape = new myShapes::Rectangle(left, len, width, type, name);
	newShape->draw(*_disp, *_board);
	_shapes.push_back(newShape);
	
}

/*function will move a shape in vector shapes by x and y given from user*/
void Menu::moveShape(int index)
{
	double x, y;
	cout << "Please enter the X moving scale :";
	cin >> x;
	cout << "Please enter the Y moving scale: ";
	cin >> y;
	Point other(x, y);
	_shapes[index]->clearDraw(*_disp, *_board);
	_shapes[index]->move(other);
	//draw all shapes
	for (int i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->draw(*_disp, *_board);
	}
}

/*function will remove a shape from vector shapes and the screen*/
void Menu::removeShape(int index)
{
	_shapes[index]->clearDraw(*_disp, *_board);
	_shapes.erase(_shapes.begin() + index);
	//draw all shapes
	for (int i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->draw(*_disp, *_board);
	}
}

/*function will delete all shapes from screen and vector shapes*/
void Menu::deleteAll()
{
	for (int i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->clearDraw(*_disp, *_board);
		delete _shapes[i];
	}
	_shapes.erase(_shapes.begin(), _shapes.end());
}