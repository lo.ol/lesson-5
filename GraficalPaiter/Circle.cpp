#include "Circle.h"
#define PI 3.14

Circle::Circle(const Point & center, double radius, const string & type, const string & name) : Shape(name, type), _radius(radius), _center(center)
{
	
}

Circle::~Circle()
{

}

const Point & Circle::getCenter() const
{
	return _center;
}

double Circle::getRadius() const
{
	return _radius;
}

void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);	
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLACK, 100.0f).display(disp);
}

void Circle::move(const Point & other)
{
	this->_center += other;
}

double Circle::getArea() const
{
	 
	return pow(_radius, 2) * PI;
}

double Circle::getPerimeter() const
{
	return PI * 2 * _radius;
}


