#include "Triangle.h"



Triangle::Triangle(const Point & a, const Point & b, const Point & c, const string & type, const string & name) : Polygon(name, type)
{
	//in case all points are in the same line triangle wont be created
	if (a.getX() == b.getX() && a.getX() == c.getX() ||
		a.getY() == b.getY() && a.getY() == c.getY())
	{
		cout << "The points entered create a line." << endl;
		cout << "Press any key to continue . . ." << endl;
		getchar();
		(*this).~Triangle();
	}
	else
	{
		//initialize points vector
		_points.push_back(a);
		_points.push_back(b);
		_points.push_back(c);
	}
}

Triangle::~Triangle()
{
	_points.clear();

}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}

double Triangle::getArea() const
{
	/*Heron's Formula:
	https://en.wikipedia.org/wiki/Heron%27s_formula */

	double s = (*this).getPerimeter() / 2;
	double area = sqrt(s*(s- _points[0].distance(_points[1]))*(s - _points[0].distance(_points[2]))*(s- _points[1].distance(_points[2])));
	return area;
}


double Triangle::getPerimeter() const
{
	/*perimeter is distance between all points*/
	double perimeter = _points[0].distance(_points[1]);
	perimeter += _points[0].distance(_points[2]);
	perimeter += _points[1].distance(_points[2]);
	return perimeter;
}


